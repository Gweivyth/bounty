<?php

$gvtbild = mysqli_real_escape_string($con, 'achievements/images/drk.png');
$star = mysqli_real_escape_string($con, '<img src="images/star.png" alt="" width="14" height="14">');


$getall = "SELECT * FROM chars WHERE charid=".$acrow['charid']."";
$getall2 = mysqli_query($con, $getall);
	$getrow = mysqli_fetch_array($getall2);
		$idchar = $getrow['charid'];
		$namechar = $getrow['charname'];



$getaward = $con->query("SELECT * FROM char_jobs where charid=".$idchar."");
	$getaward2 = mysqli_fetch_array($getaward);

	$job = $getaward2['drk'];


		
		if ($getrow['gmlevel'] < 2) {
		
			if ($job >= 1) {
				$query12 = "SELECT * FROM achievements WHERE charid=".$idchar." and typ='drkunlock'";
				$query13 = mysqli_query($con, $query12);
					if (mysqli_num_rows($query13) == 0) {
							$xp = mysqli_query($con, "UPDATE chars set points=points+10 where charid=".$idchar."");
					}
				$ach_dist = mysqli_query($con, 'INSERT INTO achievements (charid, charname, award, image, points, date, typ, color)
								SELECT * FROM (SELECT "'.$idchar.'", "'.$namechar.'", "unlocked the Advanced Job: Dark Knight.", "'.$gvtbild.'", 10,CURRENT_TIMESTAMP, "drkunlock", "#e69500") AS tmp
								WHERE NOT EXISTS 
								(SELECT charid, typ FROM achievements WHERE charid="'.$idchar.'" and typ="drkunlock")');
			}
		
			if ($job >= 10) {
				$query12 = "SELECT * FROM achievements WHERE charid=".$idchar." and typ='drk10'";
				$query13 = mysqli_query($con, $query12);
					if (mysqli_num_rows($query13) == 0) {
							$xp = mysqli_query($con, "UPDATE chars set points=points+5 where charid=".$idchar."");
					}
				$ach_dist = mysqli_query($con, 'INSERT INTO achievements (charid, charname, award, image, points, date, typ)
								SELECT * FROM (SELECT "'.$idchar.'", "'.$namechar.'", "attained level 10 as a Dark Knight.", "'.$gvtbild.'", 5,CURRENT_TIMESTAMP, "drk10") AS tmp
								WHERE NOT EXISTS 
								(SELECT charid, typ FROM achievements WHERE charid="'.$idchar.'" and typ="drk10")');
			}
			
			if ($job >= 20) {
				$query12 = "SELECT * FROM achievements WHERE charid=".$idchar." and typ='drk20'";
				$query13 = mysqli_query($con, $query12);
					if (mysqli_num_rows($query13) == 0) {
							$xp = mysqli_query($con, "UPDATE chars set points=points+5 where charid=".$idchar."");
					}
				$ach_dist = mysqli_query($con, 'INSERT INTO achievements (charid, charname, award, image, points, date, typ)
								SELECT * FROM (SELECT "'.$idchar.'", "'.$namechar.'", "attained level 20 as a Dark Knight.", "'.$gvtbild.'", 5,CURRENT_TIMESTAMP, "drk20") AS tmp
								WHERE NOT EXISTS 
								(SELECT charid, typ FROM achievements WHERE charid="'.$idchar.'" and typ="drk20")');
			}
			
			if ($job >= 30) {
				$query12 = "SELECT * FROM achievements WHERE charid=".$idchar." and typ='drk30'";
				$query13 = mysqli_query($con, $query12);
					if (mysqli_num_rows($query13) == 0) {
							$xp = mysqli_query($con, "UPDATE chars set points=points+5 where charid=".$idchar."");
					}
				$ach_dist = mysqli_query($con, 'INSERT INTO achievements (charid, charname, award, image, points, date, typ)
								SELECT * FROM (SELECT "'.$idchar.'", "'.$namechar.'", "attained level 30 as a Dark Knight.", "'.$gvtbild.'", 5,CURRENT_TIMESTAMP, "drk30") AS tmp
								WHERE NOT EXISTS 
								(SELECT charid, typ FROM achievements WHERE charid="'.$idchar.'" and typ="drk30")');
			}
			
			if ($job >= 40) {
				$query12 = "SELECT * FROM achievements WHERE charid=".$idchar." and typ='drk40'";
				$query13 = mysqli_query($con, $query12);
					if (mysqli_num_rows($query13) == 0) {
							$xp = mysqli_query($con, "UPDATE chars set points=points+5 where charid=".$idchar."");
					}
				$ach_dist = mysqli_query($con, 'INSERT INTO achievements (charid, charname, award, image, points, date, typ)
								SELECT * FROM (SELECT "'.$idchar.'", "'.$namechar.'", "attained level 40 as a Dark Knight.", "'.$gvtbild.'", 5,CURRENT_TIMESTAMP, "drk40") AS tmp
								WHERE NOT EXISTS 
								(SELECT charid, typ FROM achievements WHERE charid="'.$idchar.'" and typ="drk40")');
			}
			
			if ($job >= 50) {
				$query12 = "SELECT * FROM achievements WHERE charid=".$idchar." and typ='drk50'";
				$query13 = mysqli_query($con, $query12);
					if (mysqli_num_rows($query13) == 0) {
							$xp = mysqli_query($con, "UPDATE chars set points=points+5 where charid=".$idchar."");
					}
				$ach_dist = mysqli_query($con, 'INSERT INTO achievements (charid, charname, award, image, points, date, typ)
								SELECT * FROM (SELECT "'.$idchar.'", "'.$namechar.'", "attained level 50 as a Dark Knight.", "'.$gvtbild.'", 5,CURRENT_TIMESTAMP, "drk50") AS tmp
								WHERE NOT EXISTS 
								(SELECT charid, typ FROM achievements WHERE charid="'.$idchar.'" and typ="drk50")');
			}
			
			if ($job >= 60) {
				$query12 = "SELECT * FROM achievements WHERE charid=".$idchar." and typ='drk60'";
				$query13 = mysqli_query($con, $query12);
					if (mysqli_num_rows($query13) == 0) {
							$xp = mysqli_query($con, "UPDATE chars set points=points+5 where charid=".$idchar."");
					}
				$ach_dist = mysqli_query($con, 'INSERT INTO achievements (charid, charname, award, image, points, date, typ)
								SELECT * FROM (SELECT "'.$idchar.'", "'.$namechar.'", "attained level 60 as a Dark Knight.", "'.$gvtbild.'", 5,CURRENT_TIMESTAMP, "drk60") AS tmp
								WHERE NOT EXISTS 
								(SELECT charid, typ FROM achievements WHERE charid="'.$idchar.'" and typ="drk60")');
			}
			
			if ($job >= 70) {
				$query12 = "SELECT * FROM achievements WHERE charid=".$idchar." and typ='drk70'";
				$query13 = mysqli_query($con, $query12);
					if (mysqli_num_rows($query13) == 0) {
							$xp = mysqli_query($con, "UPDATE chars set points=points+5 where charid=".$idchar."");
					}
				$ach_dist = mysqli_query($con, 'INSERT INTO achievements (charid, charname, award, image, points, date, typ)
								SELECT * FROM (SELECT "'.$idchar.'", "'.$namechar.'", "attained level 70 as a Dark Knight.", "'.$gvtbild.'", 5,CURRENT_TIMESTAMP, "drk70") AS tmp
								WHERE NOT EXISTS 
								(SELECT charid, typ FROM achievements WHERE charid="'.$idchar.'" and typ="drk70")');
			}
			
			if ($job >= 75) {
				$query12 = "SELECT * FROM achievements WHERE charid=".$idchar." and typ='drk75'";
				$query13 = mysqli_query($con, $query12);
					if (mysqli_num_rows($query13) == 0) {
							$xp = mysqli_query($con, "UPDATE chars set points=points+10 where charid=".$idchar."");
					}
				$ach_dist = mysqli_query($con, 'INSERT INTO achievements (charid, charname, award, image, points, date, typ, color)
								SELECT * FROM (SELECT "'.$idchar.'", "'.$namechar.'", "attained level 75 as a Dark Knight.", "'.$gvtbild.'", 10,CURRENT_TIMESTAMP, "drk75", "#3399ff") AS tmp
								WHERE NOT EXISTS 
								(SELECT charid, typ FROM achievements WHERE charid="'.$idchar.'" and typ="drk75")');
			}
		}

?>