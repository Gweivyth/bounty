<?php

$gvtbild = mysqli_real_escape_string($con, 'achievements/images/leathercraft.png');
$star = mysqli_real_escape_string($con, '<img src="images/star.png" alt="" width="14" height="14">');



$getall = "SELECT * FROM chars WHERE charid=".$acrow['charid']."";
$getall2 = mysqli_query($con, $getall);
	$getrow = mysqli_fetch_array($getall2);
		$idchar = $getrow['charid'];
		$namechar = $getrow['charname'];

$getaward = $con->query("SELECT * FROM char_skills where charid=".$idchar." and skillid=53");
	$getaward2 = mysqli_fetch_array($getaward);

	$job = $getaward2['value'];


		
		if ($getrow['gmlevel'] < 2) {
		
			if ($job >= 100) {
				$query12 = "SELECT * FROM achievements WHERE charid=".$idchar." and typ='crafting_leather_10'";
				$query13 = mysqli_query($con, $query12);
					if (mysqli_num_rows($query13) == 0) {
							$xp = mysqli_query($con, "UPDATE chars set points=points+10 where charid=".$idchar."");
					}
				$ach_dist = mysqli_query($con, 'INSERT INTO achievements (charid, charname, award, image, points, date, typ)
								SELECT * FROM (SELECT "'.$idchar.'", "'.$namechar.'", "reached skill 10 in Leathercraft.", "'.$gvtbild.'", 10,CURRENT_TIMESTAMP, "crafting_leather_10") AS tmp
								WHERE NOT EXISTS 
								(SELECT charid, typ FROM achievements WHERE charid="'.$idchar.'" and typ="crafting_leather_10")');
			}
			
			if ($job >= 200) {
				$query12 = "SELECT * FROM achievements WHERE charid=".$idchar." and typ='crafting_leather_20'";
				$query13 = mysqli_query($con, $query12);
					if (mysqli_num_rows($query13) == 0) {
							$xp = mysqli_query($con, "UPDATE chars set points=points+10 where charid=".$idchar."");
					}
				$ach_dist = mysqli_query($con, 'INSERT INTO achievements (charid, charname, award, image, points, date, typ)
								SELECT * FROM (SELECT "'.$idchar.'", "'.$namechar.'", "reached skill 20 in Leathercraft.", "'.$gvtbild.'", 10,CURRENT_TIMESTAMP, "crafting_leather_20") AS tmp
								WHERE NOT EXISTS 
								(SELECT charid, typ FROM achievements WHERE charid="'.$idchar.'" and typ="crafting_leather_20")');
			}
			
			if ($job >= 300) {
				$query12 = "SELECT * FROM achievements WHERE charid=".$idchar." and typ='crafting_leather_30'";
				$query13 = mysqli_query($con, $query12);
					if (mysqli_num_rows($query13) == 0) {
							$xp = mysqli_query($con, "UPDATE chars set points=points+10 where charid=".$idchar."");
					}
				$ach_dist = mysqli_query($con, 'INSERT INTO achievements (charid, charname, award, image, points, date, typ)
								SELECT * FROM (SELECT "'.$idchar.'", "'.$namechar.'", "reached skill 30 in Leathercraft.", "'.$gvtbild.'", 10,CURRENT_TIMESTAMP, "crafting_leather_30") AS tmp
								WHERE NOT EXISTS 
								(SELECT charid, typ FROM achievements WHERE charid="'.$idchar.'" and typ="crafting_leather_30")');
			}
			
			if ($job >= 400) {
				$query12 = "SELECT * FROM achievements WHERE charid=".$idchar." and typ='crafting_leather_40'";
				$query13 = mysqli_query($con, $query12);
					if (mysqli_num_rows($query13) == 0) {
							$xp = mysqli_query($con, "UPDATE chars set points=points+10 where charid=".$idchar."");
					}
				$ach_dist = mysqli_query($con, 'INSERT INTO achievements (charid, charname, award, image, points, date, typ)
								SELECT * FROM (SELECT "'.$idchar.'", "'.$namechar.'", "reached skill 40 in Leathercraft.", "'.$gvtbild.'", 10,CURRENT_TIMESTAMP, "crafting_leather_40") AS tmp
								WHERE NOT EXISTS 
								(SELECT charid, typ FROM achievements WHERE charid="'.$idchar.'" and typ="crafting_leather_40")');
			}
			
			if ($job >= 500) {
				$query12 = "SELECT * FROM achievements WHERE charid=".$idchar." and typ='crafting_leather_50'";
				$query13 = mysqli_query($con, $query12);
					if (mysqli_num_rows($query13) == 0) {
							$xp = mysqli_query($con, "UPDATE chars set points=points+10 where charid=".$idchar."");
					}
				$ach_dist = mysqli_query($con, 'INSERT INTO achievements (charid, charname, award, image, points, date, typ)
								SELECT * FROM (SELECT "'.$idchar.'", "'.$namechar.'", "reached skill 50 in Leathercraft.", "'.$gvtbild.'", 10,CURRENT_TIMESTAMP, "crafting_leather_50") AS tmp
								WHERE NOT EXISTS 
								(SELECT charid, typ FROM achievements WHERE charid="'.$idchar.'" and typ="crafting_leather_50")');
			}
			
			if ($job >= 600) {
				$query12 = "SELECT * FROM achievements WHERE charid=".$idchar." and typ='crafting_leather_60'";
				$query13 = mysqli_query($con, $query12);
					if (mysqli_num_rows($query13) == 0) {
							$xp = mysqli_query($con, "UPDATE chars set points=points+10 where charid=".$idchar."");
					}
				$ach_dist = mysqli_query($con, 'INSERT INTO achievements (charid, charname, award, image, points, date, typ)
								SELECT * FROM (SELECT "'.$idchar.'", "'.$namechar.'", "reached skill 60 in Leathercraft.", "'.$gvtbild.'", 10,CURRENT_TIMESTAMP, "crafting_leather_60") AS tmp
								WHERE NOT EXISTS 
								(SELECT charid, typ FROM achievements WHERE charid="'.$idchar.'" and typ="crafting_leather_60")');
			}
			
			if ($job >= 700) {
				$query12 = "SELECT * FROM achievements WHERE charid=".$idchar." and typ='crafting_leather_70'";
				$query13 = mysqli_query($con, $query12);
					if (mysqli_num_rows($query13) == 0) {
							$xp = mysqli_query($con, "UPDATE chars set points=points+10 where charid=".$idchar."");
					}
				$ach_dist = mysqli_query($con, 'INSERT INTO achievements (charid, charname, award, image, points, date, typ)
								SELECT * FROM (SELECT "'.$idchar.'", "'.$namechar.'", "reached skill 70 in Leathercraft.", "'.$gvtbild.'", 10,CURRENT_TIMESTAMP, "crafting_leather_70") AS tmp
								WHERE NOT EXISTS 
								(SELECT charid, typ FROM achievements WHERE charid="'.$idchar.'" and typ="crafting_leather_70")');
			}
			
			if ($job >= 800) {
				$query12 = "SELECT * FROM achievements WHERE charid=".$idchar." and typ='crafting_leather_80'";
				$query13 = mysqli_query($con, $query12);
					if (mysqli_num_rows($query13) == 0) {
							$xp = mysqli_query($con, "UPDATE chars set points=points+10 where charid=".$idchar."");
					}
				$ach_dist = mysqli_query($con, 'INSERT INTO achievements (charid, charname, award, image, points, date, typ)
								SELECT * FROM (SELECT "'.$idchar.'", "'.$namechar.'", "reached skill 80 in Leathercraft.", "'.$gvtbild.'", 10,CURRENT_TIMESTAMP, "crafting_leather_80") AS tmp
								WHERE NOT EXISTS 
								(SELECT charid, typ FROM achievements WHERE charid="'.$idchar.'" and typ="crafting_leather_80")');
			}
			
			if ($job >= 900) {
				$query12 = "SELECT * FROM achievements WHERE charid=".$idchar." and typ='crafting_leather_90'";
				$query13 = mysqli_query($con, $query12);
					if (mysqli_num_rows($query13) == 0) {
							$xp = mysqli_query($con, "UPDATE chars set points=points+10 where charid=".$idchar."");
					}
				$ach_dist = mysqli_query($con, 'INSERT INTO achievements (charid, charname, award, image, points, date, typ)
								SELECT * FROM (SELECT "'.$idchar.'", "'.$namechar.'", "reached skill 90 in Leathercraft.", "'.$gvtbild.'", 10,CURRENT_TIMESTAMP, "crafting_leather_90") AS tmp
								WHERE NOT EXISTS 
								(SELECT charid, typ FROM achievements WHERE charid="'.$idchar.'" and typ="crafting_leather_90")');
			}
			
			if ($job >= 1000) {
				$query12 = "SELECT * FROM achievements WHERE charid=".$idchar." and typ='crafting_leather_100'";
				$query13 = mysqli_query($con, $query12);
					if (mysqli_num_rows($query13) == 0) {
							$xp = mysqli_query($con, "UPDATE chars set points=points+25 where charid=".$idchar."");
					}
				$ach_dist = mysqli_query($con, 'INSERT INTO achievements (charid, charname, award, image, points, date, typ, color)
								SELECT * FROM (SELECT "'.$idchar.'", "'.$namechar.'", "reached skill 100 in Leathercraft.", "'.$gvtbild.'", 25,CURRENT_TIMESTAMP, "crafting_leather_100", "#3399ff") AS tmp
								WHERE NOT EXISTS 
								(SELECT charid, typ FROM achievements WHERE charid="'.$idchar.'" and typ="crafting_leather_100")');
			}
		}
	
?>