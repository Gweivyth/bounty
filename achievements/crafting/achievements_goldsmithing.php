<?php

$gvtbild = mysqli_real_escape_string($con, 'achievements/images/goldsmithing.png');
$star = mysqli_real_escape_string($con, '<img src="images/star.png" alt="" width="14" height="14">');




$getall = "SELECT * FROM chars WHERE charid=".$acrow['charid']."";
$getall2 = mysqli_query($con, $getall);
	$getrow = mysqli_fetch_array($getall2);
		$idchar = $getrow['charid'];
		$namechar = $getrow['charname'];

$getaward = $con->query("SELECT * FROM char_skills where charid=".$idchar." and skillid=51");
	$getaward2 = mysqli_fetch_array($getaward);

	$job = $getaward2['value'];


		
		if ($getrow['gmlevel'] < 2) {
		
			if ($job >= 100) {
				$query12 = "SELECT * FROM achievements WHERE charid=".$idchar." and typ='crafting_goldsmith_10'";
				$query13 = mysqli_query($con, $query12);
					if (mysqli_num_rows($query13) == 0) {
							$xp = mysqli_query($con, "UPDATE chars set points=points+10 where charid=".$idchar."");
					}
				$ach_dist = mysqli_query($con, 'INSERT INTO achievements (charid, charname, award, image, points, date, typ)
								SELECT * FROM (SELECT "'.$idchar.'", "'.$namechar.'", "reached skill 10 in Goldsmithing.", "'.$gvtbild.'", 10,CURRENT_TIMESTAMP, "crafting_goldsmith_10") AS tmp
								WHERE NOT EXISTS 
								(SELECT charid, typ FROM achievements WHERE charid="'.$idchar.'" and typ="crafting_goldsmith_10")');
			}
			
			if ($job >= 200) {
				$query12 = "SELECT * FROM achievements WHERE charid=".$idchar." and typ='crafting_goldsmith_20'";
				$query13 = mysqli_query($con, $query12);
					if (mysqli_num_rows($query13) == 0) {
							$xp = mysqli_query($con, "UPDATE chars set points=points+10 where charid=".$idchar."");
					}
				$ach_dist = mysqli_query($con, 'INSERT INTO achievements (charid, charname, award, image, points, date, typ)
								SELECT * FROM (SELECT "'.$idchar.'", "'.$namechar.'", "reached skill 20 in Goldsmithing.", "'.$gvtbild.'", 10,CURRENT_TIMESTAMP, "crafting_goldsmith_20") AS tmp
								WHERE NOT EXISTS 
								(SELECT charid, typ FROM achievements WHERE charid="'.$idchar.'" and typ="crafting_goldsmith_20")');
			}
			
			if ($job >= 300) {
				$query12 = "SELECT * FROM achievements WHERE charid=".$idchar." and typ='crafting_goldsmith_30'";
				$query13 = mysqli_query($con, $query12);
					if (mysqli_num_rows($query13) == 0) {
							$xp = mysqli_query($con, "UPDATE chars set points=points+10 where charid=".$idchar."");
					}
				$ach_dist = mysqli_query($con, 'INSERT INTO achievements (charid, charname, award, image, points, date, typ)
								SELECT * FROM (SELECT "'.$idchar.'", "'.$namechar.'", "reached skill 30 in Goldsmithing.", "'.$gvtbild.'", 10,CURRENT_TIMESTAMP, "crafting_goldsmith_30") AS tmp
								WHERE NOT EXISTS 
								(SELECT charid, typ FROM achievements WHERE charid="'.$idchar.'" and typ="crafting_goldsmith_30")');
			}
			
			if ($job >= 400) {
				$query12 = "SELECT * FROM achievements WHERE charid=".$idchar." and typ='crafting_goldsmith_40'";
				$query13 = mysqli_query($con, $query12);
					if (mysqli_num_rows($query13) == 0) {
							$xp = mysqli_query($con, "UPDATE chars set points=points+10 where charid=".$idchar."");
					}
				$ach_dist = mysqli_query($con, 'INSERT INTO achievements (charid, charname, award, image, points, date, typ)
								SELECT * FROM (SELECT "'.$idchar.'", "'.$namechar.'", "reached skill 40 in Goldsmithing.", "'.$gvtbild.'", 10,CURRENT_TIMESTAMP, "crafting_goldsmith_40") AS tmp
								WHERE NOT EXISTS 
								(SELECT charid, typ FROM achievements WHERE charid="'.$idchar.'" and typ="crafting_goldsmith_40")');
			}
			
			if ($job >= 500) {
				$query12 = "SELECT * FROM achievements WHERE charid=".$idchar." and typ='crafting_goldsmith_50'";
				$query13 = mysqli_query($con, $query12);
					if (mysqli_num_rows($query13) == 0) {
							$xp = mysqli_query($con, "UPDATE chars set points=points+10 where charid=".$idchar."");
					}
				$ach_dist = mysqli_query($con, 'INSERT INTO achievements (charid, charname, award, image, points, date, typ)
								SELECT * FROM (SELECT "'.$idchar.'", "'.$namechar.'", "reached skill 50 in Goldsmithing.", "'.$gvtbild.'", 10,CURRENT_TIMESTAMP, "crafting_goldsmith_50") AS tmp
								WHERE NOT EXISTS 
								(SELECT charid, typ FROM achievements WHERE charid="'.$idchar.'" and typ="crafting_goldsmith_50")');
			}
			
			if ($job >= 600) {
				$query12 = "SELECT * FROM achievements WHERE charid=".$idchar." and typ='crafting_goldsmith_60'";
				$query13 = mysqli_query($con, $query12);
					if (mysqli_num_rows($query13) == 0) {
							$xp = mysqli_query($con, "UPDATE chars set points=points+10 where charid=".$idchar."");
					}
				$ach_dist = mysqli_query($con, 'INSERT INTO achievements (charid, charname, award, image, points, date, typ)
								SELECT * FROM (SELECT "'.$idchar.'", "'.$namechar.'", "reached skill 60 in Goldsmithing.", "'.$gvtbild.'", 10,CURRENT_TIMESTAMP, "crafting_goldsmith_60") AS tmp
								WHERE NOT EXISTS 
								(SELECT charid, typ FROM achievements WHERE charid="'.$idchar.'" and typ="crafting_goldsmith_60")');
			}
			
			if ($job >= 700) {
				$query12 = "SELECT * FROM achievements WHERE charid=".$idchar." and typ='crafting_goldsmith_70'";
				$query13 = mysqli_query($con, $query12);
					if (mysqli_num_rows($query13) == 0) {
							$xp = mysqli_query($con, "UPDATE chars set points=points+10 where charid=".$idchar."");
					}
				$ach_dist = mysqli_query($con, 'INSERT INTO achievements (charid, charname, award, image, points, date, typ)
								SELECT * FROM (SELECT "'.$idchar.'", "'.$namechar.'", "reached skill 70 in Goldsmithing.", "'.$gvtbild.'", 10,CURRENT_TIMESTAMP, "crafting_goldsmith_70") AS tmp
								WHERE NOT EXISTS 
								(SELECT charid, typ FROM achievements WHERE charid="'.$idchar.'" and typ="crafting_goldsmith_70")');
			}
			
			if ($job >= 800) {
				$query12 = "SELECT * FROM achievements WHERE charid=".$idchar." and typ='crafting_goldsmith_80'";
				$query13 = mysqli_query($con, $query12);
					if (mysqli_num_rows($query13) == 0) {
							$xp = mysqli_query($con, "UPDATE chars set points=points+10 where charid=".$idchar."");
					}
				$ach_dist = mysqli_query($con, 'INSERT INTO achievements (charid, charname, award, image, points, date, typ)
								SELECT * FROM (SELECT "'.$idchar.'", "'.$namechar.'", "reached skill 80 in Goldsmithing.", "'.$gvtbild.'", 10,CURRENT_TIMESTAMP, "crafting_goldsmith_80") AS tmp
								WHERE NOT EXISTS 
								(SELECT charid, typ FROM achievements WHERE charid="'.$idchar.'" and typ="crafting_goldsmith_80")');
			}
			
			if ($job >= 900) {
				$query12 = "SELECT * FROM achievements WHERE charid=".$idchar." and typ='crafting_goldsmith_90'";
				$query13 = mysqli_query($con, $query12);
					if (mysqli_num_rows($query13) == 0) {
							$xp = mysqli_query($con, "UPDATE chars set points=points+10 where charid=".$idchar."");
					}
				$ach_dist = mysqli_query($con, 'INSERT INTO achievements (charid, charname, award, image, points, date, typ)
								SELECT * FROM (SELECT "'.$idchar.'", "'.$namechar.'", "reached skill 90 in Goldsmithing.", "'.$gvtbild.'", 10,CURRENT_TIMESTAMP, "crafting_goldsmith_90") AS tmp
								WHERE NOT EXISTS 
								(SELECT charid, typ FROM achievements WHERE charid="'.$idchar.'" and typ="crafting_goldsmith_90")');
			}
			
			if ($job >= 1000) {
				$query12 = "SELECT * FROM achievements WHERE charid=".$idchar." and typ='crafting_goldsmith_100'";
				$query13 = mysqli_query($con, $query12);
					if (mysqli_num_rows($query13) == 0) {
							$xp = mysqli_query($con, "UPDATE chars set points=points+25 where charid=".$idchar."");
					}
				$ach_dist = mysqli_query($con, 'INSERT INTO achievements (charid, charname, award, image, points, date, typ, color)
								SELECT * FROM (SELECT "'.$idchar.'", "'.$namechar.'", "reached skill 100 in Goldsmithing.", "'.$gvtbild.'", 25,CURRENT_TIMESTAMP, "crafting_goldsmith_100", "#3399ff") AS tmp
								WHERE NOT EXISTS 
								(SELECT charid, typ FROM achievements WHERE charid="'.$idchar.'" and typ="crafting_goldsmith_100")');
			}
		}
		
?>