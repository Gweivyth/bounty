<?php

$gvtbild = mysqli_real_escape_string($con, 'achievements/images/chocobo.png');
$star = mysqli_real_escape_string($con, '<img src="images/star.png" alt="" width="14" height="14">');




$getall = "SELECT * FROM chars WHERE charid=".$acrow['charid']."";
$getall2 = mysqli_query($con, $getall);
	$getrow = mysqli_fetch_array($getall2);
		$idchar = $getrow['charid'];
		$namechar = $getrow['charname'];
		$pt = $getrow['playtime'];

$rank = "SELECT * FROM char_profile where charid=".$idchar."";
$rank2 = mysqli_query($con, $rank);
$rank3 = mysqli_fetch_array($rank2);
	$rs = $rank3['rank_sandoria'];
	$rb = $rank3['rank_bastok'];
	$rw = $rank3['rank_windurst'];

$getaward = $con->query("SELECT * FROM char_vars where charid=".$idchar." and varname='chocobolicense'");
	$getaward2 = mysqli_fetch_array($getaward);

	$chocobo = $getaward2['value'];

$fenrir = $con->query("SELECT * FROM char_vars where charid=".$idchar." and varname='JM5'");
	$fenrir2 = mysqli_fetch_array($fenrir);
	
	$tulfaire = $con->query("SELECT * FROM char_vars where charid=".$idchar." and varname='Ascended'");
	$tulfaire2 = mysqli_fetch_array($tulfaire);
	
$curl = $con->query("SELECT * FROM char_history where charid=".$idchar."");
	$curl2 = mysqli_fetch_array($curl);
	
$raaz = $con->query("SELECT SUM(value) as totskill FROM char_skills where charid=".$idchar." and skillid>=48 and skillid<=56");
	$raaz2 = mysqli_fetch_array($raaz);
		
		if ($getrow['gmlevel'] < 2) {
			
			if ($raaz2['totskill'] >= 7000) {
				$query12 = "SELECT * FROM achievements WHERE charid=".$idchar." and typ='mount_raaz'";
				$query13 = mysqli_query($con, $query12);
					if (mysqli_num_rows($query13) == 0) {
							$xp = mysqli_query($con, "UPDATE chars set points=points+50 where charid=".$idchar."");
					}
				$ach_dist = mysqli_query($con, 'INSERT INTO achievements (charid, charname, award, image, points, date, typ)
								SELECT * FROM (SELECT "'.$idchar.'", "'.$namechar.'", "obtained the Raaz Companion mount.", "'.$gvtbild.'", 50,CURRENT_TIMESTAMP, "mount_raaz") AS tmp
								WHERE NOT EXISTS 
								(SELECT charid, typ FROM achievements WHERE charid="'.$idchar.'" and typ="mount_raaz")');
						$mount = "INSERT INTO char_vars (charid,varname,value) VALUES ('".$idchar."','MOUNT_raaz',1)";
						$mount2 = mysqli_query($con, $mount);
			}
			
			if ($curl2['distance_travelled'] >= 10000000) {
				$query12 = "SELECT * FROM achievements WHERE charid=".$idchar." and typ='mount_hippo'";
				$query13 = mysqli_query($con, $query12);
					if (mysqli_num_rows($query13) == 0) {
							$xp = mysqli_query($con, "UPDATE chars set points=points+50 where charid=".$idchar."");
					}
				$ach_dist = mysqli_query($con, 'INSERT INTO achievements (charid, charname, award, image, points, date, typ)
								SELECT * FROM (SELECT "'.$idchar.'", "'.$namechar.'", "obtained the Hippogryph Companion mount.", "'.$gvtbild.'", 50,CURRENT_TIMESTAMP, "mount_hippo") AS tmp
								WHERE NOT EXISTS 
								(SELECT charid, typ FROM achievements WHERE charid="'.$idchar.'" and typ="mount_hippo")');
						$mount = "INSERT INTO char_vars (charid,varname,value) VALUES ('".$idchar."','MOUNT_hippo',1)";
						$mount2 = mysqli_query($con, $mount);
			}
			
			if ($rs == 10 and $rb == 10 and $rw == 10) {
				$query12 = "SELECT * FROM achievements WHERE charid=".$idchar." and typ='mount_byakko'";
				$query13 = mysqli_query($con, $query12);
					if (mysqli_num_rows($query13) == 0) {
							$xp = mysqli_query($con, "UPDATE chars set points=points+50 where charid=".$idchar."");
					}
				$ach_dist = mysqli_query($con, 'INSERT INTO achievements (charid, charname, award, image, points, date, typ)
								SELECT * FROM (SELECT "'.$idchar.'", "'.$namechar.'", "obtained the Byakko Companion mount.", "'.$gvtbild.'", 50,CURRENT_TIMESTAMP, "mount_byakko") AS tmp
								WHERE NOT EXISTS 
								(SELECT charid, typ FROM achievements WHERE charid="'.$idchar.'" and typ="mount_byakko")');
						$mount = "INSERT INTO char_vars (charid,varname,value) VALUES ('".$idchar."','MOUNT_byakko',1)";
						$mount2 = mysqli_query($con, $mount);
			}
			
			if ($pt >= 8640000) {
				$query12 = "SELECT * FROM achievements WHERE charid=".$idchar." and typ='mount_xomit'";
				$query13 = mysqli_query($con, $query12);
					if (mysqli_num_rows($query13) == 0) {
							$xp = mysqli_query($con, "UPDATE chars set points=points+50 where charid=".$idchar."");
					}
				$ach_dist = mysqli_query($con, 'INSERT INTO achievements (charid, charname, award, image, points, date, typ)
								SELECT * FROM (SELECT "'.$idchar.'", "'.$namechar.'", "obtained the Xzomit Companion mount.", "'.$gvtbild.'", 50,CURRENT_TIMESTAMP, "mount_xomit") AS tmp
								WHERE NOT EXISTS 
								(SELECT charid, typ FROM achievements WHERE charid="'.$idchar.'" and typ="mount_xomit")');
						$mount = "INSERT INTO char_vars (charid,varname,value) VALUES ('".$idchar."','MOUNT_xomit',1)";
						$mount2 = mysqli_query($con, $mount);
			}
			
			if ($curl2['enemies_defeated'] >= 10000) {
				$query12 = "SELECT * FROM achievements WHERE charid=".$idchar." and typ='mount_curl'";
				$query13 = mysqli_query($con, $query12);
					if (mysqli_num_rows($query13) == 0) {
							$xp = mysqli_query($con, "UPDATE chars set points=points+50 where charid=".$idchar."");
					}
				$ach_dist = mysqli_query($con, 'INSERT INTO achievements (charid, charname, award, image, points, date, typ)
								SELECT * FROM (SELECT "'.$idchar.'", "'.$namechar.'", "obtained the Coeurl Companion mount.", "'.$gvtbild.'", 50,CURRENT_TIMESTAMP, "mount_curl") AS tmp
								WHERE NOT EXISTS 
								(SELECT charid, typ FROM achievements WHERE charid="'.$idchar.'" and typ="mount_curl")');
						$mount = "INSERT INTO char_vars (charid,varname,value) VALUES ('".$idchar."','MOUNT_curl',1)";
						$mount2 = mysqli_query($con, $mount);
			}
			
			if ($fenrir2['value'] == 1) {
				$query12 = "SELECT * FROM achievements WHERE charid=".$idchar." and typ='mount_fenrir'";
				$query13 = mysqli_query($con, $query12);
					if (mysqli_num_rows($query13) == 0) {
							$xp = mysqli_query($con, "UPDATE chars set points=points+50 where charid=".$idchar."");
					}
				$ach_dist = mysqli_query($con, 'INSERT INTO achievements (charid, charname, award, image, points, date, typ, color)
								SELECT * FROM (SELECT "'.$idchar.'", "'.$namechar.'", "obtained the Fenrir Companion mount.", "'.$gvtbild.'", 50,CURRENT_TIMESTAMP, "mount_fenrir", "#3399ff") AS tmp
								WHERE NOT EXISTS 
								(SELECT charid, typ FROM achievements WHERE charid="'.$idchar.'" and typ="mount_fenrir")');
						$mount = "INSERT INTO char_vars (charid,varname,value) VALUES ('".$idchar."','MOUNT_fenrir',1)";
						$mount2 = mysqli_query($con, $mount);
			}
			
			if ($tulfaire2['value'] >= 1) {
				$query12 = "SELECT * FROM achievements WHERE charid=".$idchar." and typ='mount_tulfaire'";
				$query13 = mysqli_query($con, $query12);
					if (mysqli_num_rows($query13) == 0) {
							$xp = mysqli_query($con, "UPDATE chars set points=points+50 where charid=".$idchar."");
					}
				$ach_dist = mysqli_query($con, 'INSERT INTO achievements (charid, charname, award, image, points, date, typ, color)
								SELECT * FROM (SELECT "'.$idchar.'", "'.$namechar.'", "obtained the Tulfaire Companion mount.", "'.$gvtbild.'", 50,CURRENT_TIMESTAMP, "mount_tulfaire", "#3399ff") AS tmp
								WHERE NOT EXISTS 
								(SELECT charid, typ FROM achievements WHERE charid="'.$idchar.'" and typ="mount_tulfaire")');
						$mount = "INSERT INTO char_vars (charid,varname,value) VALUES ('".$idchar."','MOUNT_tulfaire',1)";
						$mount2 = mysqli_query($con, $mount);
			}
		
			if ($chocobo == 1) {
				$query12 = "SELECT * FROM achievements WHERE charid=".$idchar." and typ='mount_chocobo'";
				$query13 = mysqli_query($con, $query12);
					if (mysqli_num_rows($query13) == 0) {
							$xp = mysqli_query($con, "UPDATE chars set points=points+50 where charid=".$idchar."");
					}
				$ach_dist = mysqli_query($con, 'INSERT INTO achievements (charid, charname, award, image, points, date, typ)
								SELECT * FROM (SELECT "'.$idchar.'", "'.$namechar.'", "obtained the Chocobo Companion mount.", "'.$gvtbild.'", 50,CURRENT_TIMESTAMP, "mount_chocobo") AS tmp
								WHERE NOT EXISTS 
								(SELECT charid, typ FROM achievements WHERE charid="'.$idchar.'" and typ="mount_chocobo")');
			}
			
			if ($getrow['level'] >= 10) { 
				$query12 = "SELECT * FROM achievements WHERE charid=".$idchar." and typ='mount_chocobo'";
				$query13 = mysqli_query($con, $query12);
					if (mysqli_num_rows($query13) == 0) {
							$xp = mysqli_query($con, "UPDATE chars set points=points+50 where charid=".$idchar."");
					}
				$ach_dist = mysqli_query($con, 'INSERT INTO achievements (charid, charname, award, image, points, date, typ)
								SELECT * FROM (SELECT "'.$idchar.'", "'.$namechar.'", "obtained the Chocobo Companion mount.", "'.$gvtbild.'", 50,CURRENT_TIMESTAMP, "mount_chocobo") AS tmp
								WHERE NOT EXISTS 
								(SELECT charid, typ FROM achievements WHERE charid="'.$idchar.'" and typ="mount_chocobo")');
						$mount = "INSERT INTO char_vars (charid,varname,value) VALUES ('".$idchar."','MOUNT_chocobo',1)";
						$mount2 = mysqli_query($con, $mount);
			}
			
			if ($getrow['level'] >= 30) { 
				$query12 = "SELECT * FROM achievements WHERE charid=".$idchar." and typ='mount_raptor'";
				$query13 = mysqli_query($con, $query12);
					if (mysqli_num_rows($query13) == 0) {
							$xp = mysqli_query($con, "UPDATE chars set points=points+50 where charid=".$idchar."");
					}
				$ach_dist = mysqli_query($con, 'INSERT INTO achievements (charid, charname, award, image, points, date, typ)
								SELECT * FROM (SELECT "'.$idchar.'", "'.$namechar.'", "obtained the Raptor Companion mount.", "'.$gvtbild.'", 50,CURRENT_TIMESTAMP, "mount_raptor") AS tmp
								WHERE NOT EXISTS 
								(SELECT charid, typ FROM achievements WHERE charid="'.$idchar.'" and typ="mount_raptor")');
						$mount = "INSERT INTO char_vars (charid,varname,value) VALUES ('".$idchar."','MOUNT_raptor',1)";
						$mount2 = mysqli_query($con, $mount);
			}
			
			if ($getrow['level'] >= 50) { 
				$query12 = "SELECT * FROM achievements WHERE charid=".$idchar." and typ='mount_omega'";
				$query13 = mysqli_query($con, $query12);
					if (mysqli_num_rows($query13) == 0) {
							$xp = mysqli_query($con, "UPDATE chars set points=points+50 where charid=".$idchar."");
					}
				$ach_dist = mysqli_query($con, 'INSERT INTO achievements (charid, charname, award, image, points, date, typ, color)
								SELECT * FROM (SELECT "'.$idchar.'", "'.$namechar.'", "obtained the Omega Companion mount.", "'.$gvtbild.'", 50,CURRENT_TIMESTAMP, "mount_omega", "#3399ff") AS tmp
								WHERE NOT EXISTS 
								(SELECT charid, typ FROM achievements WHERE charid="'.$idchar.'" and typ="mount_omega")');
						$mount = "INSERT INTO char_vars (charid,varname,value) VALUES ('".$idchar."','MOUNT_omega',1)";
						$mount2 = mysqli_query($con, $mount);
			}
			
		}
		
?>