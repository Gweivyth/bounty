<?php
//$host = "localhost"; /* Host name */
//$user = "root"; /* User */
//$password = "jonas123"; /* Password */
//$dbname = "xiweb"; /* Database name */

//$con = mysqli_connect($host, $user, $password,$dbname);
//mysqli_set_charset($con,"utf8");
$gvtbild = mysqli_real_escape_string($con, 'achievements/images/game-controller.png');
$star = mysqli_real_escape_string($con, '<img src="images/star.png" alt="" width="14" height="14">');


//$acc = "SELECT * FROM accounts_sessions";
//$acc2 = mysqli_query($con, $acc);
//	while($acrow = $acc2->fetch_array()) {

$getall = "SELECT * FROM chars WHERE charid=".$acrow['charid']."";
$getall2 = mysqli_query($con, $getall);
	$getrow = mysqli_fetch_array($getall2);
		$idchar = $getrow['charid'];
		$namechar = $getrow['charname'];

//$getaward = $con->query("SELECT * FROM char_vars where charid=".$idchar." and varname='chocobolicense'");
	//$getaward2 = mysqli_fetch_array($getaward);

	$chocobo = $getrow['playtime'];

		
		if ($getrow['gmlevel'] < 2) {
		
			if ($chocobo > 3600) { // 1 hour
				$query12 = "SELECT * FROM achievements WHERE charid=".$idchar." and typ='playtime_1h'";
				$query13 = mysqli_query($con, $query12);
					if (mysqli_num_rows($query13) == 0) {
							$xp = mysqli_query($con, "UPDATE chars set points=points+5 where charid=".$idchar."");
					}
				$ach_dist = mysqli_query($con, 'INSERT INTO achievements (charid, charname, award, image, points, date, typ)
								SELECT * FROM (SELECT "'.$idchar.'", "'.$namechar.'", "reached the playtime of 1 hour.", "'.$gvtbild.'", 5,CURRENT_TIMESTAMP, "playtime_1h") AS tmp
								WHERE NOT EXISTS 
								(SELECT charid, typ FROM achievements WHERE charid="'.$idchar.'" and typ="playtime_1h")');
			}
			
			if ($chocobo > 21600) { // 6 hours
				$query12 = "SELECT * FROM achievements WHERE charid=".$idchar." and typ='playtime_6h'";
				$query13 = mysqli_query($con, $query12);
					if (mysqli_num_rows($query13) == 0) {
							$xp = mysqli_query($con, "UPDATE chars set points=points+5 where charid=".$idchar."");
					}
				$ach_dist = mysqli_query($con, 'INSERT INTO achievements (charid, charname, award, image, points, date, typ)
								SELECT * FROM (SELECT "'.$idchar.'", "'.$namechar.'", "reached the playtime of 6 hours.", "'.$gvtbild.'", 5,CURRENT_TIMESTAMP, "playtime_6h") AS tmp
								WHERE NOT EXISTS 
								(SELECT charid, typ FROM achievements WHERE charid="'.$idchar.'" and typ="playtime_6h")');
			}
			
			if ($chocobo > 43200) { // 12 hours
				$query12 = "SELECT * FROM achievements WHERE charid=".$idchar." and typ='playtime_12h'";
				$query13 = mysqli_query($con, $query12);
					if (mysqli_num_rows($query13) == 0) {
							$xp = mysqli_query($con, "UPDATE chars set points=points+5 where charid=".$idchar."");
					}
				$ach_dist = mysqli_query($con, 'INSERT INTO achievements (charid, charname, award, image, points, date, typ)
								SELECT * FROM (SELECT "'.$idchar.'", "'.$namechar.'", "reached the playtime of 12 hours.", "'.$gvtbild.'", 5,CURRENT_TIMESTAMP, "playtime_12h") AS tmp
								WHERE NOT EXISTS 
								(SELECT charid, typ FROM achievements WHERE charid="'.$idchar.'" and typ="playtime_12h")');
			}
			
			if ($chocobo > 86400) { // 1 day
				$query12 = "SELECT * FROM achievements WHERE charid=".$idchar." and typ='playtime_1d'";
				$query13 = mysqli_query($con, $query12);
					if (mysqli_num_rows($query13) == 0) {
							$xp = mysqli_query($con, "UPDATE chars set points=points+5 where charid=".$idchar."");
					}
				$ach_dist = mysqli_query($con, 'INSERT INTO achievements (charid, charname, award, image, points, date, typ)
								SELECT * FROM (SELECT "'.$idchar.'", "'.$namechar.'", "reached the playtime of 1 day.", "'.$gvtbild.'", 5,CURRENT_TIMESTAMP, "playtime_1d") AS tmp
								WHERE NOT EXISTS 
								(SELECT charid, typ FROM achievements WHERE charid="'.$idchar.'" and typ="playtime_1d")');
			}
			
			if ($chocobo > 432000) { // 5 days
				$query12 = "SELECT * FROM achievements WHERE charid=".$idchar." and typ='playtime_5d'";
				$query13 = mysqli_query($con, $query12);
					if (mysqli_num_rows($query13) == 0) {
							$xp = mysqli_query($con, "UPDATE chars set points=points+5 where charid=".$idchar."");
					}
				$ach_dist = mysqli_query($con, 'INSERT INTO achievements (charid, charname, award, image, points, date, typ)
								SELECT * FROM (SELECT "'.$idchar.'", "'.$namechar.'", "reached the playtime of 5 days.", "'.$gvtbild.'", 5,CURRENT_TIMESTAMP, "playtime_5d") AS tmp
								WHERE NOT EXISTS 
								(SELECT charid, typ FROM achievements WHERE charid="'.$idchar.'" and typ="playtime_5d")');
			}
			
			if ($chocobo > 864000) { // 10 days
				$query12 = "SELECT * FROM achievements WHERE charid=".$idchar." and typ='playtime_10d'";
				$query13 = mysqli_query($con, $query12);
					if (mysqli_num_rows($query13) == 0) {
							$xp = mysqli_query($con, "UPDATE chars set points=points+5 where charid=".$idchar."");
					}
				$ach_dist = mysqli_query($con, 'INSERT INTO achievements (charid, charname, award, image, points, date, typ)
								SELECT * FROM (SELECT "'.$idchar.'", "'.$namechar.'", "reached the playtime of 10 days.", "'.$gvtbild.'", 5,CURRENT_TIMESTAMP, "playtime_10d") AS tmp
								WHERE NOT EXISTS 
								(SELECT charid, typ FROM achievements WHERE charid="'.$idchar.'" and typ="playtime_10d")');
			}
			
			if ($chocobo > 2160000) { // 25 days
				$query12 = "SELECT * FROM achievements WHERE charid=".$idchar." and typ='playtime_25d'";
				$query13 = mysqli_query($con, $query12);
					if (mysqli_num_rows($query13) == 0) {
							$xp = mysqli_query($con, "UPDATE chars set points=points+5 where charid=".$idchar."");
					}
				$ach_dist = mysqli_query($con, 'INSERT INTO achievements (charid, charname, award, image, points, date, typ)
								SELECT * FROM (SELECT "'.$idchar.'", "'.$namechar.'", "reached the playtime of 25 days.", "'.$gvtbild.'", 5,CURRENT_TIMESTAMP, "playtime_25d") AS tmp
								WHERE NOT EXISTS 
								(SELECT charid, typ FROM achievements WHERE charid="'.$idchar.'" and typ="playtime_25d")');
			}
			
			if ($chocobo > 4320000) { // 50 days
				$query12 = "SELECT * FROM achievements WHERE charid=".$idchar." and typ='playtime_50d'";
				$query13 = mysqli_query($con, $query12);
					if (mysqli_num_rows($query13) == 0) {
							$xp = mysqli_query($con, "UPDATE chars set points=points+5 where charid=".$idchar."");
					}
				$ach_dist = mysqli_query($con, 'INSERT INTO achievements (charid, charname, award, image, points, date, typ)
								SELECT * FROM (SELECT "'.$idchar.'", "'.$namechar.'", "reached the playtime of 50 days.", "'.$gvtbild.'", 5,CURRENT_TIMESTAMP, "playtime_50d") AS tmp
								WHERE NOT EXISTS 
								(SELECT charid, typ FROM achievements WHERE charid="'.$idchar.'" and typ="playtime_50d")');
			}
			
			if ($chocobo > 6480000) { // 75 days
				$query12 = "SELECT * FROM achievements WHERE charid=".$idchar." and typ='playtime_75d'";
				$query13 = mysqli_query($con, $query12);
					if (mysqli_num_rows($query13) == 0) {
							$xp = mysqli_query($con, "UPDATE chars set points=points+5 where charid=".$idchar."");
					}
				$ach_dist = mysqli_query($con, 'INSERT INTO achievements (charid, charname, award, image, points, date, typ)
								SELECT * FROM (SELECT "'.$idchar.'", "'.$namechar.'", "reached the playtime of 75 days.", "'.$gvtbild.'", 5,CURRENT_TIMESTAMP, "playtime_75d") AS tmp
								WHERE NOT EXISTS 
								(SELECT charid, typ FROM achievements WHERE charid="'.$idchar.'" and typ="playtime_75d")');
			}
			
			if ($chocobo > 8640000) { // 100 days
				$query12 = "SELECT * FROM achievements WHERE charid=".$idchar." and typ='playtime_100d'";
				$query13 = mysqli_query($con, $query12);
					if (mysqli_num_rows($query13) == 0) {
							$xp = mysqli_query($con, "UPDATE chars set points=points+10 where charid=".$idchar."");
					}
				$ach_dist = mysqli_query($con, 'INSERT INTO achievements (charid, charname, award, image, points, date, typ, color)
								SELECT * FROM (SELECT "'.$idchar.'", "'.$namechar.'", "reached the playtime of 100 days.", "'.$gvtbild.'", 10,CURRENT_TIMESTAMP, "playtime_100d", "#3399ff") AS tmp
								WHERE NOT EXISTS 
								(SELECT charid, typ FROM achievements WHERE charid="'.$idchar.'" and typ="playtime_100d")');
			}
		}
	//}	
?>