
<!DOCTYPE html>
<html>
	<head>
	<link rel="icon" href="images/phoenix.png" type="image/png">
<?php include 'navbar.php'; ?>
	<script src="js/jquery.bpopup.min.js"></script>
		<link rel="stylesheet" href="bootstrap-5.1.3-dist/css/bootstrap.min.css"/>
		<link rel="stylesheet" href="/bootstrap-5.1.3-dist/css/css.css">
			<meta charset="utf-8">
			<!--<meta name="viewport" content="width=device-width, initial-scale=1">-->
				<title> Phoenix XI </title>
				

	</head>
<style>
.online {width:550px;border:1px solid lightgray;border-radius:5px;margin-left:auto;margin-right:5%;background:#ffffe6;}
.onlinetop {border-bottom:1px solid lightgray;color:gray;padding-left:5px;font-weight:550;}
.city {height:20px;width:20px;border-radius:5px;}
.city2 {height:15px;width:15px;border-radius:5px;}
.job {font-size:14px;letter-spacing:-1px;}
.op {width:100%;}
.ophead {text-align:center;font-size:14px;}
.ophead2 {text-align:left;border-bottom:1px solid lightgray;color:gray;}
.server {float:right;margin-right:5%;margin-bottom:-10px;font-size:18px;}
.serverstatus {float:right;margin-right:5%;margin-top:-15px;font-size:18px;}
.op th {border-bottom:1px solid lightgray;}
.viewall {float:right;padding-right:5px;color:gray;font-weight:450;}
</style>
<body>
<div class="header">
<?php
	$socket = (@fsockopen('127.0.0.1',54230, $errNo, $errString, 1.0));

  if($errNo >= 1 || !$socket){
    $status = 0;
  } else {
    $status = 1;
    //fclose($socket);
  } 
		if ($status == 0) {
			$reset = "UPDATE server set uptime=NULL WHERE id=1";
			$reset = mysqli_query($con, $reset);
		}
?>
	<font class="server">Server Status: 
		<?php if ($status == 1) { ?>  Online <img width="15" height="15" src="images/online.png"><?php } ?>
		<?php if ($status == 0) { ?>  Offline <img width="15" height="15" src="images/offline.png"><?php } ?>  
	</font>
	<h2>Phoenix XI</h2> 
	<font class="serverstatus">Server Uptime:
		<?php if ($status == 0) { ?> -<?php } ?>
		<?php if ($status == 1) { 
					
				$datime = "SELECT * FROM server WHERE id=1";
				$datime2 = mysqli_query($con, $datime);
				$datime3 = mysqli_fetch_array($datime2);
					if ($datime3['uptime'] != NULL) {
				$datime4 = $datime3['uptime'];
				$datime5 = date('Y-m-d H:i:s');

					$datetime1 = new DateTime(".$datime4.");
					$datetime2 = new DateTime(".$datime5.");

					$interval = $datetime1->diff($datetime2);
					echo '<b>';
					echo $interval->d;
					echo '</b>';
					echo ' days ';
					echo '<b>';
					echo $interval->h;
					echo '</b>';
					echo ' hours';					
					}} ?>
	</font>
	<h5>A Final Fantasy XI Private Server</h5>
</div>
<div class="redline"></div>
<style>
.awards {width:1100px;margin:0 auto;font-family: "Roboto Slab", serif;}
.awardstop h1{font-weight:700;color: #3d4449;line-height: 1.5;font-family: "Roboto Slab", serif;}
.awardscontent {display:flex;}
.awardsleft {width:550px;}
.awardsright {width:550px;}

.awardstable {background:rgba(255,255,255,0.8);width:95%;margin:0 auto;border:1px solid lightgray;font-family: "Open Sans", sans-serif;}
.at1 {border-right:1px solid lightgray;border-bottom:1px solid lightgray;width:22px;}
.at2 {border-right:1px solid lightgray;border-bottom:1px solid lightgray;width:458px;padding-left:5px;}
.at3 {border-right:1px solid lightgray;border-bottom:1px solid lightgray;width:40px;padding-left:3px;padding-right:3px;}
.at4 {border-bottom:1px solid lightgray;width:35px;padding-left:5px;}
.awimg {height:22px;width:22px;border-radius:5px;margin-bottom:1px;}
.awlink {text-decoration:none;color:#999999;font-size:14px;}
.awlink:hover {cursor:pointer;}
.awtext {color:#8c8c8c;font-size:13px;letter-spacing:0.4px;}
.awpoints {color:gray;font-size:13px;}
.awdate {font-size:12px;color:gray;}

.pagnation {text-align:center;margin-top:10px;}
.pagination {display:inline-block;}
.pagination li {float:left;}
.pagearrow {filter: invert(52%) sepia(9%) saturate(0%) hue-rotate(273deg) brightness(94%) contrast(87%);margin-bottom:3px;}
.page-link {color:gray !important;}
.recaw {margin-left:15px;font-size:18px;}
.awtable2 {background:none !important;border:none !important;font-weight: 400;line-height: 1.65;margin-bottom:20px;}
.awtable2 th {font-size:15px;font-weight:550;color:#4d4d4d;}
.awtable2td {background:rgba(255,255,255,0.8);font-size:14px;color:gray;border:1px solid lightgray;}
.awpoints2 {text-align:center;border-right:1px solid lightgray;}
.awawa {text-align:center;border-right:1px solid lightgray;}
.aw2cat {padding-left:5px;border-right:1px solid lightgray;}
.jmpic {height:10px;width:10px;margin-bottom:4px;margin-left:3px;}
.awnum {border-right:1px solid lightgray;text-align:center;}
.medal {height:25px;width:25px;}
.halolevel {width:25px;height:25px;margin-right:5px;}
</style>

<div class="awards">
	<div class="awardstop">
		<h1>Awards</h1>
	</div>
	<div class="awardscontent">

		<div class="awardsleft">
		
			<font class="recaw">Leaderboard</font>
			<table class="awardstable awtable2">
				<tr>
				<th style="padding-left:10px;width:5%;"></th>
				<th style="padding-left:10px;width:45%;">Player</th>
				<th style="text-align:center;width:20%;">Points</th>
				<th style="text-align:center;width:20%;">Awards</th>
				<th style="text-align:center;width:10%;">Complete</th>
				</tr>
<?php	$leader = "SELECT * FROM chars WHERE gmlevel=0 or gmlevel=1 ORDER BY points DESC LIMIT 15";
		$leader2 = mysqli_query($con, $leader);
			while($leaderrow = $leader2->fetch_array()) {
				$playnum = $playnum+1;
				
		$sumpoints = "SELECT COUNT(*) as awcount FROM achievements WHERE charid=".$leaderrow['charid']." and typ<>'level_1' and typ<>'level_2' and typ<>'level_3' and typ<>'level_4' and typ<>'level_5' and typ<>'level_6' and typ<>'level_7' and typ<>'level_8' and typ<>'level_9' and typ<>'level_10' and typ<>'level_11' and typ<>'level_12' and typ<>'level_13' and typ<>'level_14' and typ<>'level_15' and typ<>'level_16' and typ<>'level_17' and typ<>'level_18' and typ<>'level_19' and typ<>'level_20' and typ<>'level_21' and typ<>'level_22' and typ<>'level_23' and typ<>'level_24' and typ<>'level_25' and typ<>'level_26' and typ<>'level_27' and typ<>'level_28' and typ<>'level_29' and typ<>'level_30' and typ<>'level_31' and typ<>'level_32' and typ<>'level_33' and typ<>'level_34' and typ<>'level_35' and typ<>'level_36' and typ<>'level_37' and typ<>'level_38' and typ<>'level_39' and typ<>'level_40' and typ<>'level_41' and typ<>'level_42' and typ<>'level_43' and typ<>'level_44' and typ<>'level_45' and typ<>'level_46' and typ<>'level_47' and typ<>'level_48' and typ<>'level_49' and typ<>'level_50'";
		$sumpoints2 = mysqli_query($con, $sumpoints);
		$sumpoints3 = mysqli_fetch_array($sumpoints2);
		
		$percent = ($sumpoints3['awcount']*100/579);
?>
				<tr class="awtable2td">
					<td class="awnum">
						<?php 
							if ($playnum == 1) { echo '<img class="medal" src="images/medal1.png">'; }
							if ($playnum == 2) { echo '<img class="medal" src="images/medal2.png">'; }
							if ($playnum == 3) { echo '<img class="medal" src="images/medal3.png">'; }
							if ($playnum > 3) { echo $playnum; }
						?>
					</td>
					<td class="aw2cat"><a class="awlink" href="profile?player=<?php echo $leaderrow['charname'];?>"><?php echo $leaderrow['charname'];?></a>
					<font style="float:right">
					<?php
					if ($leaderrow['gmlevel'] == 0 or $leaderrow['gmlevel'] == 1) {
		if ($leaderrow['level'] == 1) { echo '<img class="halolevel" src="halolevels/1.png">'; } 
		if ($leaderrow['level'] == 2) { echo '<img class="halolevel" src="halolevels/2.png">'; } 
		if ($leaderrow['level'] == 3) { echo '<img class="halolevel" src="halolevels/3.png">'; } 
		if ($leaderrow['level'] == 4) { echo '<img class="halolevel" src="halolevels/4.png">'; } 
		if ($leaderrow['level'] == 5) { echo '<img class="halolevel" src="halolevels/5.png">'; } 
		if ($leaderrow['level'] == 6) { echo '<img class="halolevel" src="halolevels/6.png">'; } 
		if ($leaderrow['level'] == 7) { echo '<img class="halolevel" src="halolevels/7.png">'; } 
		if ($leaderrow['level'] == 8) { echo '<img class="halolevel" src="halolevels/8.png">'; } 
		if ($leaderrow['level'] == 9) { echo '<img class="halolevel" src="halolevels/9.png">'; } 
		if ($leaderrow['level'] == 10) { echo '<img class="halolevel" src="halolevels/10.png">'; } 
		if ($leaderrow['level'] == 11) { echo '<img class="halolevel" src="halolevels/11.png">'; } 
		if ($leaderrow['level'] == 12) { echo '<img class="halolevel" src="halolevels/12.png">'; } 
		if ($leaderrow['level'] == 13) { echo '<img class="halolevel" src="halolevels/13.png">'; } 
		if ($leaderrow['level'] == 14) { echo '<img class="halolevel" src="halolevels/14.png">'; } 
		if ($leaderrow['level'] == 15) { echo '<img class="halolevel" src="halolevels/15.png">'; } 
		if ($leaderrow['level'] == 16) { echo '<img class="halolevel" src="halolevels/16.png">'; } 
		if ($leaderrow['level'] == 17) { echo '<img class="halolevel" src="halolevels/17.png">'; }
		if ($leaderrow['level'] == 18) { echo '<img class="halolevel" src="halolevels/18.png">'; }
		if ($leaderrow['level'] == 19) { echo '<img class="halolevel" src="halolevels/19.png">'; }
		if ($leaderrow['level'] == 20) { echo '<img class="halolevel" src="halolevels/20.png">'; }
		if ($leaderrow['level'] == 21) { echo '<img class="halolevel" src="halolevels/21.png">'; }
		if ($leaderrow['level'] == 22) { echo '<img class="halolevel" src="halolevels/22.png">'; }
		if ($leaderrow['level'] == 23) { echo '<img class="halolevel" src="halolevels/23.png">'; }
		if ($leaderrow['level'] == 24) { echo '<img class="halolevel" src="halolevels/24.png">'; }
		if ($leaderrow['level'] == 25) { echo '<img class="halolevel" src="halolevels/25.png">'; }
		if ($leaderrow['level'] == 26) { echo '<img class="halolevel" src="halolevels/26.png">'; }
		if ($leaderrow['level'] == 27) { echo '<img class="halolevel" src="halolevels/27.png">'; }
		if ($leaderrow['level'] == 28) { echo '<img class="halolevel" src="halolevels/28.png">'; }
		if ($leaderrow['level'] == 29) { echo '<img class="halolevel" src="halolevels/29.png">'; }
		if ($leaderrow['level'] == 30) { echo '<img class="halolevel" src="halolevels/30.png">'; }
		if ($leaderrow['level'] == 31) { echo '<img class="halolevel" src="halolevels/31.png">'; }
		if ($leaderrow['level'] == 32) { echo '<img class="halolevel" src="halolevels/32.png">'; }
		if ($leaderrow['level'] == 33) { echo '<img class="halolevel" src="halolevels/33.png">'; }
		if ($leaderrow['level'] == 34) { echo '<img class="halolevel" src="halolevels/34.png">'; }
		if ($leaderrow['level'] == 35) { echo '<img class="halolevel" src="halolevels/35.png">'; }
		if ($leaderrow['level'] == 36) { echo '<img class="halolevel" src="halolevels/36.png">'; }
		if ($leaderrow['level'] == 37) { echo '<img class="halolevel" src="halolevels/37.png">'; }
		if ($leaderrow['level'] == 38) { echo '<img class="halolevel" src="halolevels/38.png">'; }
		if ($leaderrow['level'] == 39) { echo '<img class="halolevel" src="halolevels/39.png">'; }
		if ($leaderrow['level'] == 40) { echo '<img class="halolevel" src="halolevels/40.png">'; }
		if ($leaderrow['level'] == 41) { echo '<img class="halolevel" src="halolevels/41.png">'; }
		if ($leaderrow['level'] == 42) { echo '<img class="halolevel" src="halolevels/42.png">'; }
		if ($leaderrow['level'] == 43) { echo '<img class="halolevel" src="halolevels/43.png">'; }
		if ($leaderrow['level'] == 44) { echo '<img class="halolevel" src="halolevels/44.png">'; }
		if ($leaderrow['level'] == 45) { echo '<img class="halolevel" src="halolevels/45.png">'; }
		if ($leaderrow['level'] == 46) { echo '<img class="halolevel" src="halolevels/46.png">'; }
		if ($leaderrow['level'] == 47) { echo '<img class="halolevel" src="halolevels/47.png">'; }
		if ($leaderrow['level'] == 48) { echo '<img class="halolevel" src="halolevels/48.png">'; }
		if ($leaderrow['level'] == 49) { echo '<img class="halolevel" src="halolevels/49.png">'; }
		if ($leaderrow['level'] == 50) { echo '<img class="halolevel" src="halolevels/50.png">'; }
			}
		?>
					</font>
					</td>
					<td class="awpoints2"><?php echo $leaderrow['points']; ?></td>
					<td class="awawa"><?php echo $sumpoints3['awcount']; ?></td>
					<td style="text-align:center;"><?php echo round($percent,0); ?>%</td>
				</tr>
			<?php } ?>
			</table>
<?php
	$a1 = 120;		$b1 = 12;
	$a2 = 180;		$b2 = 18;
	$a3 = 750;		$b3 = 70;
	$a4 = 810;		$b4 = 144;
	$a5 = 1170;		$b5 = 90;
	$a6 = 50;		$b6 = 5;
	$a7 = 500;		$b7 = 10;
	$a8 = 270;		$b8 = 27;
	$a9 = 55;		$b9 = 10;
	$a10 = 140;		$b10 = 14;
	$a11 = 250;		$b11 = 5;
	$a12 = 400;		$b12 = 16;
	$a13 = 450;		$b13 = 18;
	$a14 = 360;		$b14 = 32;
	$a15 = 150;		$b15 = 15;
	$a16 = 150;		$b16 = 6;
	$a17 = 125;		$b17 = 5;
	$a18 = 140;		$b18 = 14;
	$a19 = 110;		$b19 = 11;
	$a20 = 800;		$b20 = 32;
	$a21 = 220;		$b21 = 25;
	
	$apoints = $a1+$a2+$a3+$a4+$a5+$a6+$a7+$a8+$a9+$a10+$a11+$a12+$a13+$a14+$a15+$a16+$a17+$a18+$a19+$a20+$a21;
	$aawards = $b1+$b2+$b3+$b4+$b5+$b6+$b7+$b8+$b9+$b10+$b11+$b12+$b13+$b14+$b15+$b16+$b17+$b18+$b19+$b20+$b21;
	
	include 'awardinfo.php';
?>

<style>
.workinfo {height:25px;width:25px;filter: invert(54%) sepia(91%) saturate(2440%) hue-rotate(6deg) brightness(95%) contrast(101%);position:relative;z-index:100;margin-left:5px;}
.workcont {text-align:left;margin-top:5px;}
.awardtype {text-decoration:underline;}
</style>
		<font class="recaw">Available Awards</font>
			<table class="awardstable awtable2">
				<tr>
				<th style="padding-left:10px;width:60%;">Category</th>
				<th style="text-align:center;width:20%;">Points</th>
				<th style="text-align:center;width:20%;">Awards</th>
				</tr>
				<tr class="awtable2td">
					<td class="aw2cat">Advanced Jobs <font style="float:right"><img id="t90" class="workinfo" src="images/info.png"></font></td>
					<td class="awpoints2">120 </td>
					<td class="awawa">12</td>
				</tr>
				<tr class="awtable2td">
					<td class="aw2cat">Artifact Equipment <font style="float:right"><img id="artifact" class="workinfo" src="images/info.png"></font></td>
					<td class="awpoints2">180</td>
					<td class="awawa">18</td>
				</tr>
				<tr class="awtable2td">
					<td class="aw2cat">Ascension <font style="float:right"><img id="ascension" class="workinfo" src="images/info.png"></font></td>
					<td class="awpoints2">250</td>
					<td class="awawa">5</td>
				</tr>
				<tr class="awtable2td">
					<td class="aw2cat">Crafting <font style="float:right"><img id="crafting" class="workinfo" src="images/info.png"></font></td>
					<td class="awpoints2">750</td>
					<td class="awawa">70</td>
				</tr>
				<tr class="awtable2td">
					<td class="aw2cat">Job Levels <font style="float:right"><img id="joblevel" class="workinfo" src="images/info.png"></font></td>
					<td class="awpoints2">810</td>
					<td class="awawa">144</td>
				</tr>
				<tr class="awtable2td">
					<td class="aw2cat">Job Mastery <font style="float:right"><img id="jobmastery" class="workinfo" src="images/info.png"></font></td>
					<td class="awpoints2">1170</td>
					<td class="awawa">90</td>
				</tr>
				<tr class="awtable2td">
					<td class="aw2cat">Job Points <font style="float:right"><img id="jp" class="workinfo" src="images/info.png"></font></td>
					<td class="awpoints2">220</td>
					<td class="awawa">25</td>
				</tr>
				<tr class="awtable2td">
					<td class="aw2cat">Limit Breaks <font style="float:right"><img id="limitbreak" class="workinfo" src="images/info.png"></font></td>
					<td class="awpoints2">50</td>
					<td class="awawa">5</td>
				</tr>
				<tr class="awtable2td">
					<td class="aw2cat">Mounts <font style="float:right"><img id="mounts" class="workinfo" src="images/info.png"></font></td>
					<td class="awpoints2">500</td>
					<td class="awawa">10</td>
				</tr>
				<tr class="awtable2td">
					<td class="aw2cat">Mythic Weapons <font style="float:right"><img id="mythic" class="workinfo" src="images/info.png"></font></td>
					<td class="awpoints2">450</td>
					<td class="awawa">18</td>
				</tr>
				<tr class="awtable2td">
					<td class="aw2cat">Nation Rank <font style="float:right"><img id="nation" class="workinfo" src="images/info.png"></font></td>
					<td class="awpoints2">270</td>
					<td class="awawa">27</td>
				</tr>
				<tr class="awtable2td">
					<td class="aw2cat">Notables <font style="float:right"><img id="notable" class="workinfo" src="images/info.png"></font></td>
					<td class="awpoints2">360</td>
					<td class="awawa">32</td>
				</tr>
				<tr class="awtable2td">
					<td class="aw2cat">Playtime <font style="float:right"><img id="playtime" class="workinfo" src="images/info.png"></font></td>
					<td class="awpoints2">55</td>
					<td class="awawa">10</td>
				</tr>
				<tr class="awtable2td">
					<td class="aw2cat">Records of Eminence <font style="float:right"><img id="roe" class="workinfo" src="images/info.png"></font></td>
					<td class="awpoints2">110</td>
					<td class="awawa">11</td>
				</tr>
				<tr class="awtable2td">
					<td class="aw2cat">Relic Weapons <font style="float:right"><img id="relic" class="workinfo" src="images/info.png"></font></td>
					<td class="awpoints2">400</td>
					<td class="awawa">16</td>
				</tr>
				<tr class="awtable2td">
					<td class="aw2cat">Storage <font style="float:right"><img id="storage" class="workinfo" src="images/info.png"></font></td>
					<td class="awpoints2">140</td>
					<td class="awawa">14</td>
				</tr>
				<tr class="awtable2td">
					<td class="aw2cat">Titles <font style="float:right"><img id="titles" class="workinfo" src="images/info.png"></font></td>
					<td class="awpoints2">150</td>
					<td class="awawa">15</td>
				</tr>
				<tr class="awtable2td">
					<td class="aw2cat">Trusts <font style="float:right"><img id="trust" class="workinfo" src="images/info.png"></font></td>
					<td class="awpoints2">150</td>
					<td class="awawa">6</td>
				</tr>
				<tr class="awtable2td">
					<td class="aw2cat">Salvage <font style="float:right"><img id="salvage" class="workinfo" src="images/info.png"></font></td>
					<td class="awpoints2">125</td>
					<td class="awawa">5</td>
				</tr>
				<tr class="awtable2td">
					<td class="aw2cat">Weaponskills <font style="float:right"><img id="ws" class="workinfo" src="images/info.png"></font></td>
					<td class="awpoints2">140</td>
					<td class="awawa">14</td>
				</tr>
				<tr class="awtable2td">
					<td class="aw2cat">Empyrean Gear <font style="float:right"><img id="emp" class="workinfo" src="images/info.png"></font></td>
					<td class="awpoints2">800</td>
					<td class="awawa">32</td>
				</tr>
				
				<tr class="awtable2td">
					<td class="aw2cat"></td>
					<td class="awpoints2"><b><?php echo $apoints; ?></b></td>
					<td class="awawa"><b><?php echo $aawards; ?></b></td>
				</tr>
			</table>
		</div>
		<div class="awardsright">
		<font class="recaw">Recent Awards</font>
			<table class="awardstable">
<?php
if (isset($_GET['pageno'])) {
    $pageno = $_GET['pageno'];
} else {
    $pageno = 1;
}
$no_of_records_per_page = 33;
$offset = ($pageno-1) * $no_of_records_per_page; 
$total_pages_sql = "SELECT COUNT(*) FROM achievements";
$result = mysqli_query($con,$total_pages_sql);
$total_rows = mysqli_fetch_array($result)[0];
$total_pages = ceil($total_rows / $no_of_records_per_page);

	$aw = "SELECT * FROM achievements ORDER BY id DESC LIMIT $offset, $no_of_records_per_page";
	$aw2 = mysqli_query($con, $aw);
		while($awrow = $aw2->fetch_array()) {
?>
			<tr>
				<td class="at1"><img class="awimg" src="<?php echo $awrow['image'];?>"></td>
				<td class="at2"><a class="awlink" href="profile?player=<?php echo $awrow['charname'];?>" style="color:<?php echo $awrow['color'];?> !important;" ><?php echo $awrow['charname'];?></a> <font class="awtext" style="color:<?php echo $awrow['color'];?> !important;" ><?php echo $awrow['award'];?></font></td>
				<td class="at3"><font class="awdate"  style="color:<?php echo $awrow['color'];?> !important;" ><?php echo date("F.d", strtotime($awrow['date']));?></font></td>
				<td class="at4"><font class="awpoints" style="color:<?php echo $awrow['color'];?> !important;" ><?php echo $awrow['points'];?>p</font></td>
			</tr>
		<?php } ?>
			</table>
		<div class="pagnation">
			<ul class="pagination">
				<li class="page-item"><a  class="page-link" href="?pageno=1">1</a></li>
				<li class="page-item <?php if($pageno <= 1){ echo 'disabled'; } ?>">
					<a   class="page-link" href="<?php if($pageno <= 1){ echo '#'; } else { echo "?pageno=".($pageno - 1); } ?>"><img  class="pagearrow" height="14px" width="14px" src="images/left.png"></a>
				</li>
				<li class="page-item disabled"><a   class="page-link" href="?pageno=<?php echo $total_pages; ?>"><?php echo $pageno; ?></a></li>
				<li class="page-item <?php if($pageno >= $total_pages){ echo 'disabled'; } ?>">
					<a   class="page-link" href="<?php if($pageno >= $total_pages){ echo '#'; } else { echo "?pageno=".($pageno + 1); } ?>"><img class="pagearrow" height="14px" width="14px" src="images/right.png"></a>
				</li>
				<li class="page-item"><a   class="page-link" href="?pageno=<?php echo $total_pages; ?>"><?php echo $total_pages; ?></a></li>
			</ul>
		</div>
		</div>
	</div>
</div>
<div style="height:100px;"> </div>
</body>
</html>